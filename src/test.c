#define _DEFAULT_SOURCE
#define HEAP_SIZE 1024

#include "mem.h"
#include "mem_internals.h"
#include "test.h"
#include "util.h"

void debug(const char *fmt, ...);

static void send_err(bool result, const char *msg) { if (!result) err(msg); }

static void print_heap_state(struct block_header *heap, const char *msg) {
    debug("%s\n", msg);
    debug_heap(stderr, heap);
    debug("\n");
}

static void* test_last_block( struct block_header const* block ) {
  return  (void*) (block->contents + block->capacity.bytes);
}


static struct block_header *get_header (void *data) {
    return (struct block_header *) ((uint8_t *) data - offsetof(struct block_header, contents));
}

void test1(struct block_header *heap) {
    int64_t *array = _malloc(32);
    send_err(array == NULL, "Test 1: failed. _malloc return null");
    debug("Test 1: success\n\n");

    print_heap_state(heap, "Heap state is:");

    _free(array);
}

void test2(struct block_header *heap) {
    print_heap_state(heap, "Heap state is");

    debug("Allocate 2 memory blocks:");
    void *data1 = _malloc(3000); struct block_header *data1_block = get_header(data1);
    void *data2 = _malloc(1500); struct block_header *data2_block = get_header(data2);
    send_err(data1 != NULL && data2 != NULL, "Test 2: failed. _malloc return NULL");
    send_err(!data1_block->is_free && !data2_block->is_free, "Test 2: failed. Block status is false");
    send_err(data1_block->capacity.bytes == 3000 && data2_block->capacity.bytes == 1500, "Test 2: failed. Unexpected capacity value");
    debug("Test 2: success");

    print_heap_state(heap, "Heap state is:");
    
    debug("Free first memory block:");
    send_err(data1_block->is_free, "Test 2: failed. Block status is incorrect");
    send_err(!data2_block->is_free, "Test 2: failed. Block status is incorrect");
    debug("Test 2: success\n\n");
    
     print_heap_state(heap, "Heap state is");
   
    _free(data1);
    _free(data2);
}

void test3(struct block_header *heap) {

    print_heap_state(heap, "Heap state is:");
    
    debug("Allocate 3 memory blocks:\n");
    void *data1 = _malloc(3000); struct block_header *data1_block = get_header(data1);
    void *data2 = _malloc(1500); struct block_header *data2_block = get_header(data2);
    void *data3 = _malloc(1700); struct block_header *data3_block = get_header(data3);
    send_err(data1 != NULL && data2 != NULL && data3 != NULL, "Test 3: failed. _malloc return NULL");
    send_err(
        !data1_block->is_free && 
        !data2_block->is_free &&
        !data3_block->is_free, "");
    send_err(
        data1_block->capacity.bytes == 3000 && 
        data2_block->capacity.bytes == 1500 &&
        data3_block->capacity.bytes == 1700, "Test 3: failed. Unexpected capacity value");
    debug("Test 3: success");

    print_heap_state(heap, "Heap state is:");
    
    debug("Free 2 memory blocks:\n");
    _free(data1);
    _free(data3);
    send_err(data1_block->is_free, "Test 3: failed.  Block status is incorrect\n");
    send_err(data3_block->is_free, "Test 3: failed.  Block status is incorrect\n");
    send_err(!data2_block->is_free, "Test 3: failed.  Block status is incorrect\n");
    debug("Test 3: success\n\n");

    print_heap_state(heap, "Heap state is:");

    _free(data2);
}

void test4(struct block_header *heap) {
    print_heap_state(heap, "Heap state is:");
    
    debug("Allocate 3 memory blocks:\n");

    void *data1 = _malloc(10000); struct block_header *data1_block = get_header(data1);
    void *data2 = _malloc(5000); struct block_header *data2_block = get_header(data2);
    void *data3 = _malloc(10000); struct block_header *data3_block = get_header(data3);
    send_err(
        data1 != NULL &&
        data2 != NULL &&
        data3 != NULL, "Test 4: failed. _malloc return NULL");
    send_err(
        !data1_block->is_free && 
        !data2_block->is_free &&
        !data3_block->is_free, "Test 4: failed. Block status is incorrect\n");
    send_err(
        data1_block->capacity.bytes == 10000 && 
        data2_block->capacity.bytes == 5000 &&
        data3_block->capacity.bytes == 10000, "Test 4: failed. Unexpected capacity value\n");
    send_err(
        (uint8_t *)data1_block->contents + data1_block->capacity.bytes == (uint8_t *)data2_block &&
        (uint8_t *)data2_block->contents + data2_block->capacity.bytes == (uint8_t *)data3_block, "Test 4: failed. New region      wasn't created after last");
    debug("Success\n\n");


      print_heap_state(heap, "Heap state is:");
    
    
    _free(data3);
    _free(data2);
    _free(data1);

}



void test5(struct block_header *heap) {

    struct block_header *last_block = heap;

    print_heap_state(heap, "Heap state is:");
   
    void *data1 = _malloc(10000); 
    struct block_header *data1_block = get_header(data1);
    debug("Allocate big memory block:\n"); 
    send_err(data1 != NULL, "Test 5: failed. _malloc return null");
    send_err(!data1_block->is_free, "Test 5: failed. Block status is incorrect\n");
    send_err(data1_block->capacity.bytes == 10000, "Test 5: failed. Unexpected capacity value\n");
    debug("Success\n\n");
    while (last_block->next != NULL) last_block = last_block->next;
    void *map_res = mmap(test_last_block(last_block), 1000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
    send_err(map_res != MAP_FAILED, "Test 5: failed. mmap return `MAP_FAILED`");

    debug("Allocate bigger memory block:\n");
    void *data2 = _malloc(40000); struct block_header *data2_block = get_header(data2);
    send_err(data1 != NULL, "Test 5: failed. _malloc return null");
    send_err(!data2_block->is_free, "Test 5: failed. Block status is incorrect\n");
    send_err(data2_block->capacity.bytes == 40000, "Test 5: failed. Unexpected capacity value\n");
    send_err(
        (uint8_t *)data1_block->contents + data1_block->capacity.bytes != (uint8_t *)data2_block, "Test 5: failed. New block wasn't allocated at new place");
    debug("Success\n\n");

    print_heap_state(heap, "Heap state is:");

    _free(data2);
    _free(data1);
}


void test() {
    struct block_header *heap = (struct block_header *)heap_init(HEAP_SIZE);

    test1(heap);
    test2(heap);
    test3(heap);
    test4(heap);
    test5(heap);

    munmap(HEAP_START, HEAP_SIZE);
}
  
    
